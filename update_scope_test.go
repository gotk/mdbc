package mdbc

import (
	"context"
	"fmt"
	"testing"

	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
)

func TestUpdateScope_One(t *testing.T) {
	cfg := &Config{
		URI:         "mongodb://admin:admin@10.0.0.135:27017/admin",
		MinPoolSize: 32,
		ConnTimeout: 10,
	}
	cfg.RegistryBuilder = RegisterTimestampCodec(nil)
	client, err := ConnInit(cfg)

	if err != nil {
		logrus.Fatalf("get err: %+v", err)
	}
	Init(client).InitDB(client.Database("mdbc"))

	var m = NewModel(&ModelSchedTask{})

	var record = ModelSchedTask{Id: "0e434c7d5c9eb8b129ce42ce07afef09"}
	if err := m.SetDebug(true).FindOne().SetFilter(bson.M{"_id": record.Id}).Get(&record); err != nil {
		logrus.Errorf("get err: %+v", err)
	}

	fmt.Printf("record %+v\n", record)
	var ctx = context.Background()

	record.TaskState = uint32(TaskState_TaskStateCompleted)

	updateData := bson.M{
		"task_state": uint32(TaskState_TaskStateFailed),
	}
	_, err = m.SetDebug(true).Update().SetContext(ctx).SetFilter(bson.M{"_id": record.Id}).One(updateData)
	if err != nil {
		logrus.Errorf("get err: %+v", err)
	}

	updateData = bson.M{
		"task_state": uint32(TaskState_TaskStateFailed),
	}
	_, err = m.SetDebug(true).Update().SetFilter(bson.M{"_id": record.Id}).One(updateData)
	if err != nil {
		logrus.Errorf("get err: %+v", err)
	}
}

func TestUpdateScope_Many(t *testing.T) {
	cfg := &Config{
		URI:         "mongodb://mdbc:mdbc@10.0.0.135:27117/admin",
		MinPoolSize: 32,
		ConnTimeout: 10,
	}
	cfg.RegistryBuilder = RegisterTimestampCodec(nil)
	client, err := ConnInit(cfg)

	if err != nil {
		logrus.Fatalf("get err: %+v", err)
	}
	Init(client).InitDB(client.Database("mdbc"))

	var m = NewModel(&ModelSchedTask{})

	//updateData := bson.M{
	//	"$inc": bson.M{
	//		"set_group_name": 1,
	//	},
	//}
	res, err := m.SetDebug(true).Update().SetFilter(bson.M{"task_type": "set_group_name"}).MustMapMany(m)
	if err != nil {
		logrus.Errorf("get err: %+v", err)
		panic(err)
	}
	logrus.Infof("%+v", res)
}
