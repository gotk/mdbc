package mdbc

import (
	"testing"
	"time"

	"go.mongodb.org/mongo-driver/mongo/readpref"

	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
)

func TestFindOneScope(t *testing.T) {

	client, err := ConnInit(&Config{
		URI:             "mongodb://mdbc:mdbc@10.0.0.135:27117/admin",
		MinPoolSize:     32,
		ConnTimeout:     10,
		ReadPreference:  readpref.Nearest(),
		RegistryBuilder: RegisterTimestampCodec(nil),
	})
	if err != nil {
		logrus.Fatalf("get err: %+v", err)
	}
	InitDB(client.Database("mdbc"))

	time.Sleep(time.Second * 5)

	var m = NewModel(&ModelSchedTask{})

	var record ModelSchedTask
	err = m.SetDebugError(true).FindOne().SetFilter(bson.M{"_id": "insertffdddfknkodsanfkasdf"}).Get(&record)
	if err != nil {
		return
	}
	logrus.Infof("get: %+v", &record)

	time.Sleep(time.Second * 5)
}

func TestFindOneScope_Delete(t *testing.T) {
	var m = NewModel(&ModelSchedTask{})
	err := m.SetCacheExpiredAt(time.Second*300).FindOne().
		SetFilter(bson.M{"_id": "13123"}).SetCacheFunc("Id", DefaultFindOneCacheFunc()).Delete()
	if err != nil {
		panic(err)
	}
	logrus.Infof("get ttl: %+v", m.cache.ttl)
}

func TestFindOneScope_Replace(t *testing.T) {
	var m = NewModel(&ModelSchedTask{})
	var record ModelSchedTask
	err := m.FindOne().
		SetFilter(bson.M{"_id": "0e63f5962e18a8da331289caaa3fa224"}).Get(&record)
	record.RspJson = "hahahahah"
	err = m.FindOne().SetFilter(bson.M{"_id": record.Id}).Replace(&record)
	if err != nil {
		panic(err)
	}
	logrus.Infof("get ttl: %+v", m.cache.ttl)
}
