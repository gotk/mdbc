// Package builder 用来快速构建 aggregate 查询
package builder

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type Builder struct{}

type PipelineBuilder struct {
	pipeline mongo.Pipeline
}

type IndexBuilder struct {
	d bson.D
}

type QueryBuilder struct {
	q bson.M
}

func NewBuilder() *Builder {
	return &Builder{}
}

// Pipeline 构建器
func (b *Builder) Pipeline() *PipelineBuilder {
	return &PipelineBuilder{pipeline: mongo.Pipeline{}}
}

// Index key构建器
func (b *Builder) Index() *IndexBuilder {
	return &IndexBuilder{}
}

// Query 查询构建器
func (b *Builder) Query() *QueryBuilder {
	return &QueryBuilder{}
}
