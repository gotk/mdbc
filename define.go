package mdbc

import (
	"time"

	"go.mongodb.org/mongo-driver/bson"

	"gitlab.com/gotk/gotk/core"
)

const defaultBreakerTime = time.Second * 5

const (
	// ErrFilterParamEmpty Query参数为空
	ErrFilterParamEmpty = 20001
	// ErrUpdateObjectEmpty 更新参数为空
	ErrUpdateObjectEmpty = 20002
	// ErrUpdateObjectNotSupport 更新参数类型不支持
	ErrUpdateObjectNotSupport = 20003
	// ErrOpTransaction 事务执行错误
	ErrOpTransaction = 20004
	// ErrObjectTypeNoMatch 获取对象类型不正确
	ErrObjectTypeNoMatch = 20005
)

var (
	coreCodeMap = map[int32]string{
		ErrFilterParamEmpty:       "query param is empty",
		ErrUpdateObjectEmpty:      "update object is empty",
		ErrUpdateObjectNotSupport: "update object type not support",
		ErrOpTransaction:          "abort transaction failed",
		ErrObjectTypeNoMatch:      "find object type not support",
	}
)

var (
	ErrRecordNotFound = core.ErrMsg{
		ErrCode: core.ErrRecordNotFound,
		ErrMsg:  core.GetErrMsg(core.ErrRecordNotFound),
	}

	ErrRequestBroken = core.ErrMsg{
		ErrCode: core.ErrRequestBroken,
		ErrMsg:  core.GetErrMsg(core.ErrRequestBroken),
	}

	ErrFilterEmpty = core.ErrMsg{
		ErrCode: ErrFilterParamEmpty,
		ErrMsg:  core.GetErrMsg(ErrFilterParamEmpty),
	}

	ErrObjectEmpty = core.ErrMsg{
		ErrCode: ErrUpdateObjectEmpty,
		ErrMsg:  core.GetErrMsg(ErrUpdateObjectEmpty),
	}

	ErrUpdateObjectTypeNotSupport = core.ErrMsg{
		ErrCode: ErrUpdateObjectNotSupport,
		ErrMsg:  core.GetErrMsg(ErrUpdateObjectNotSupport),
	}

	ErrRollbackTransaction = core.ErrMsg{
		ErrCode: ErrOpTransaction,
		ErrMsg:  core.GetErrMsg(ErrOpTransaction),
	}

	ErrFindObjectTypeNotSupport = core.ErrMsg{
		ErrCode: ErrObjectTypeNoMatch,
		ErrMsg:  core.GetErrMsg(ErrObjectTypeNoMatch),
	}
)

func register() {
	core.RegisterError(coreCodeMap)
}

type M bson.M

type A bson.A

type D bson.D

type E bson.E

type action string

const (
	Aggregate         action = "Aggregate"
	BulkWrite         action = "BulkWrite"
	CountDocuments    action = "Count"
	DeleteOne         action = "DeleteOne"
	DeleteMany        action = "DeleteMany"
	Distinct          action = "Distinct"
	Drop              action = "Drop"
	Find              action = "Find"
	FindOne           action = "FindOne"
	FindOneAndDelete  action = "FindOneAndDelete"
	FindOneAndReplace action = "FindOneAndReplace"
	FindOneAndUpdate  action = "FindOneAndUpdate"
	InsertOne         action = "InsertOne"
	InsertMany        action = "InsertMany"
	Indexes           action = "Indexes"
	ReplaceOne        action = "ReplaceOne"
	UpdateOne         action = "UpdateOne"
	UpdateMany        action = "UpdateMany"
	Watch             action = "Watch"
)
