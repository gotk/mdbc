package mdbc

import "reflect"

type StructField struct {
	StructFieldName string
	DbFieldName     string
}

type tabler interface {
	TableName() string
}

// GetTableName 获取表名
func GetTableName(i interface{}) string {
	if p, ok := i.(tabler); ok {
		return p.TableName()
	}
	return ""
}

// isReflectNumber 检测是否是数值类型
func isReflectNumber(obj reflect.Value) bool {
	var k = obj.Kind()
	if k == reflect.Int || k == reflect.Int8 || k == reflect.Int16 || k == reflect.Int32 ||
		k == reflect.Int64 || k == reflect.Uint || k == reflect.Uint8 || k == reflect.Uint16 ||
		k == reflect.Uint32 || k == reflect.Uint64 || k == reflect.Float32 || k == reflect.Float64 {
		return true
	}
	return false
}

// getReflectTypeField 获取j指定的字段的field 没有获取到则返回原字段名称
func getBsonTagByReflectTypeField(rt reflect.Type, fieldName string) string {
	for i := 0; i < rt.NumField(); i++ {
		field := rt.Field(i)
		if field.Name != fieldName {
			continue
		}
		return field.Tag.Get("bson")
	}
	return fieldName
}
