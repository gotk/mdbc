package mdbc

// breakerDo db熔断相关的告警处理
// 熔断应该记录执行时间 和单位时间间隔内执行次数
// 这里执行时间已经记录 单位时间间隔内执行次数暂未统计
type breakerDo interface {
	doReporter() string // 告警 返回告警内容
}

// BreakerReporter 告警器
type BreakerReporter struct {
	reportTitle     string
	reportMsg       string
	reportErrorWrap error
	breakerDo
}

// Report 告警，向Slack发出告警信息
func (b *BreakerReporter) Report() {

}
