package mdbc

//func TestTransaction(t *testing.T) {
//	cfg := &Config{
//		URI:         "mongodb://10.0.0.135:27117/mdbc",
//		MinPoolSize: 32,
//		ConnTimeout: 10,
//	}
//	cfg.RegistryBuilder = RegisterTimestampCodec(nil)
//	client, err := ConnInit(cfg)
//
//	if err != nil {
//		logrus.Fatalf("get err: %+v", err)
//	}
//	var m = NewModel(&ModelWsConnectRecord{})
//
//	ctx := context.Background()
//
//	cli := client.Client
//	stx, err := cli.StartSession()
//	if err != nil {
//		panic(err)
//	}
//
//	// ef6e39c011ee4ed8bd0f82efb0fbccee
//	// 80a850d0d8584668b89809e1308a9878
//	// 457188bf227341e5a4a6c3d0242d425a
//	_, uuid := stx.ID().Lookup("id").Binary()
//	logrus.Infof("id: %+v", hex.EncodeToString(uuid))
//
//	col := client.Database("mdbc").Collection(m.GetTableName())
//
//	if err := stx.StartTransaction(); err != nil {
//		panic(err)
//	}
//
//	logrus.Infof("id: %+v", stx.ID().Lookup("id").String())
//
//	err = mongo.WithSession(ctx, stx, func(tx mongo.SessionContext) error {
//		logrus.Infof("id: %+v", tx.ID())
//		var record ModelWsConnectRecord
//		if err := col.FindOne(tx, bson.M{"_id": "87c9c841b078bd213c79b682ffbdbec7"}).Decode(&record); err != nil {
//			logrus.Errorf("err: %+v", err)
//			panic(err)
//		}
//
//		logrus.Infof("record: %+v", &record)
//
//		record.BindId = "myidsssssssssss"
//		if err := col.FindOneAndUpdate(tx, bson.M{"_id": "87c9c841b078bd213c79b682ffbdbec7"}, bson.M{
//			"$set": &record,
//		}).Err(); err != nil {
//			if err := stx.AbortTransaction(tx); err != nil {
//				panic(err)
//			}
//
//			return err
//		}
//
//		if err := stx.CommitTransaction(tx); err != nil {
//			panic(err)
//		}
//
//		logrus.Infof("id: %+v", tx.ID().Lookup("id").String())
//
//		return nil
//	})
//
//	if err != nil {
//		panic(err)
//	}
//
//	stx.EndSession(ctx)
//}
//
//func TestNewTransaction(t *testing.T) {
//	cfg := &Config{
//		URI:         "mongodb://mdbc:mdbc@10.0.0.135:27117/mdbc",
//		MinPoolSize: 32,
//		ConnTimeout: 10,
//	}
//	cfg.RegistryBuilder = RegisterTimestampCodec(nil)
//	client, err := ConnInit(cfg)
//	client.Database("")
//
//	if err != nil {
//		logrus.Fatalf("get err: %+v", err)
//	}
//	var m = NewModel(&ModelWsConnectRecord{})
//
//	err = m.Transaction().StartSession().AddHook(&OpentracingHook{}).RollbackOnError(func(tx *TransactionScope) error {
//		ctx := tx.getSessionContext()
//		coll := tx.scope
//		var record ModelWsConnectRecord
//		if err := coll.FindOne().SetContext(ctx).SetFilter(bson.M{"_id": "d1ef466f75b9a03256a8c2a3da0409a3"}).Get(&record); err != nil {
//			logrus.Errorf("update err: %+v", err)
//			return err
//		}
//		record.BindId = uuid.New().String()
//		logrus.Infof("bind_id: %+v", record.BindId)
//		if _, err := coll.Update().SetID("d1ef466f75b9a03256a8c2a3da0409a3").One(&record); err != nil {
//			logrus.Errorf("update err: %+v", err)
//			return err
//		}
//		return nil
//	})
//	if err != nil {
//		panic(err)
//	}
//
//	time.Sleep(3 * time.Second)
//}
//
//func TestNewWithTransaction(t *testing.T) {
//	cfg := &Config{
//		URI:         "mongodb://10.0.0.135:27117/mdbc",
//		MinPoolSize: 32,
//		ConnTimeout: 10,
//	}
//	cfg.RegistryBuilder = RegisterTimestampCodec(nil)
//	client, err := ConnInit(cfg)
//	client.Database("")
//
//	if err != nil {
//		logrus.Fatalf("get err: %+v", err)
//	}
//	var m = NewModel(&ModelWsConnectRecord{})
//	err = m.Transaction().SetContext(context.Background()).StartSession().SingleRollbackOnError(func(tx *TransactionScope) error {
//		ctx := tx.getSessionContext()
//		coll := tx.scope
//		var record ModelWsConnectRecord
//		if err := coll.FindOne().SetContext(ctx).SetFilter(bson.M{"_id": "697022b263e2c1528f32a26e704e63d6"}).Get(&record); err != nil {
//			logrus.Errorf("get err: %+v", err)
//			return err
//		}
//		record.BindId = uuid.New().String()
//		logrus.Infof("bind_id: %+v", record.BindId)
//		if res, err := coll.Update().SetContext(ctx).SetID("697022b263e2c1528f32a26e704e63d6").One(&record); err != nil {
//			logrus.Errorf("update err: %+v", err)
//			return err
//		} else if res.MatchedCount != 2 {
//			//return errors.New("no match")
//			return nil
//		}
//		return nil
//	}).CloseSession().Error()
//	if err != nil {
//		panic(err)
//	}
//}
