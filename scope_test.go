package mdbc

import (
	"context"
	"fmt"
	"testing"
	"time"
)

func TestScope_check(t *testing.T) {
	var a Scope

	fmt.Println(a.execT)
}

func TestScopeCtx(t *testing.T) {
	//var ctx, cancel = context.WithTimeout(context.Background(), time.Second)
	//defer cancel()
	//time.Sleep(time.Second * 2)
	//fmt.Println(ctx.Err())

	var c1 = context.Background()
	var c2, cancel = context.WithTimeout(c1, time.Second)
	fmt.Println(c1, c2)
	cancel()
	cancel()

	fmt.Println("------")

	//fmt.Printf("c1: %p, c2: %p", &c1, &c2)
	fmt.Println(c2.Err())
}
