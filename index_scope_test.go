package mdbc

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/mongo/readpref"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func TestIndexScope_GetList(t *testing.T) {
	client, err := ConnInit(&Config{
		URI:             "mongodb://mdbc:mdbc@10.0.0.135:27117/admin",
		MinPoolSize:     32,
		ConnTimeout:     10,
		ReadPreference:  readpref.Nearest(),
		RegistryBuilder: RegisterTimestampCodec(nil),
	})
	if err != nil {
		logrus.Fatalf("get err: %+v", err)
	}
	InitDB(client.Database("mdbc"))

	var list IndexList
	var m = NewModel(&ModelSchedTask{})
	err = m.Index().SetContext(context.Background()).GetIndexList(&list)

	if err != nil {
		panic(err)
	}

	for _, card := range list {
		logrus.Infof("%+v\n", card)
	}
}

func TestIndexScope_DropAll(t *testing.T) {
	var m = NewModel(&ModelSchedTask{})
	err := m.Index().SetContext(context.Background()).DropAll()
	if err != nil {
		panic(err)
	}
}

func TestIndexScope_DropOne(t *testing.T) {
	var m = NewModel(&ModelSchedTask{})
	err := m.Index().SetContext(context.Background()).DropOne("expire_time_2")
	if err != nil {
		panic(err)
	}
}

func TestIndexScope_AddIndexModel(t *testing.T) {
	var err error

	var m = NewModel(&ModelSchedTask{})
	// 创建多个索引
	_, err = m.Index().SetContext(context.Background()).AddIndexModels(mongo.IndexModel{
		Keys:    bson.D{{Key: "expire_time", Value: 2}},   // 设置索引列
		Options: options.Index().SetExpireAfterSeconds(0), // 设置过期时间
	}).AddIndexModels(mongo.IndexModel{
		Keys: bson.D{{Key: "created_at", Value: 1}}, // 设置索引列
	}).CreateMany()

	// 创建单个索引
	_, err = m.Index().SetContext(context.Background()).AddIndexModels(mongo.IndexModel{
		Keys:    bson.D{{Key: "expire_time", Value: 2}},   // 设置索引列
		Options: options.Index().SetExpireAfterSeconds(0), // 设置过期时间
	}).CreateOne()

	if err != nil {
		panic(err)
	}
}

func TestIndexScope_SetListIndexesOption(t *testing.T) {
	var list IndexList
	s := int32(2)
	mt := 10 * time.Second
	var m = NewModel(&ModelSchedTask{})
	//设定ListIndexesOptions
	err := m.Index().SetListIndexesOption(options.ListIndexesOptions{
		BatchSize: &s,
		MaxTime:   &mt,
	}).GetIndexList(&list)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v\n", list)
}
