package mdbc

import (
	"errors"
)

var ErrorCursorIsNil = errors.New("cursor is nil")
