<img src="icon.png" alt="MDBC" width="300">

### 快速开始

初始化mongo数据库连接

```go
client, err := mongodb.ConnInit("mongodb://admin:admin@10.0.0.135:27017/admin")
if err != nil {
    logrus.Fatalf("get err: %+v", err)
}
mdbc.InitDB(client.Database("mdbc"))
```

声明 model

```go
var m = mdbc.NewModel(&ModelSchedTask{})
```

然后就可以使用 m 进行链式操作

### 注册全局对象

可以将model注册成一个全局变量
```go
type WsConnectRecordScope struct {
    *mdbc.Scope
}

var WsConnectRecord *WsConnectRecordScope

func NewWsConnectRecord() {
    WsConnectRecord = new(WsConnectRecordScope)
    WsConnectRecord.Scope = mdbc.NewModel(&model.ModelWsConnectRecord{})
}
```

使用：

```go
func beforeRemoveWs(ctx context.Context, recordID, key string) {
    if WsConnectRecord == nil {
        NewWsConnectRecord()
    }
    tm := time.Now().UnixNano() / 1e6
    if message_common.GetEtcdWatcher().RemoveWatch(key) {
        // 已经移除 变更最近的一条消息
        err := WsConnectRecord.SetContext(ctx).FindOne().SetFilter(bson.M{
            model.ModelWsConnectRecordField_Id.DbFieldName: recordID,
        }).Update(bson.M{
            "$set": bson.M{
                model.ModelWsConnectRecordField_LogoutAt.DbFieldName: tm,
            },
        })
        if err != nil {
            log.Errorf("update ws conn record err: %+v", err)
            common.Logger.Error(ctx, "WsConn", log2.String("error", err.Error()))
        }
    }
}
```
