module gitlab.com/gotk/mdbc

go 1.16

require (
	github.com/antonfisher/nested-logrus-formatter v1.3.1
	github.com/go-redis/redis/v8 v8.11.4
	github.com/google/uuid v1.3.0
	github.com/json-iterator/go v1.1.12
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/gotk/gotk v0.0.0-20220223083201-5d05a06943c3
	go.mongodb.org/mongo-driver v1.8.3
	golang.org/x/sys v0.0.0-20220222200937-f2425489ef4c // indirect
	google.golang.org/protobuf v1.27.1
)
